CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Ian Whitcomb - http://drupal.org/user/771654

This module demonstrates usage of the Data Table Field.


INSTALLATION
------------

1. Copy the datatable/ directory to your sites/SITENAME/modules directory.

3. Enable the Data Table Field and Data Table Field Example modules.
