CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Ian Whitcomb - http://drupal.org/user/771654

Data Table is an addition to the core form API which allows the creation of a
datatable field type. A data table is a draggable, sortable, table of data. Not
only does it provide a table full of editable data, but also allows the
developer to define a custom structure to display the data which taps directly
into the form API. For example, rather than showing a grid full of text boxes,
the developer can specify any of the normal form API elements to be used
instead(ie: checkboxes, select lists, text areas, etc..).


INSTALLATION
------------

1. Copy this datatable/ directory to your sites/SITENAME/modules directory.

3. Enable the module.
